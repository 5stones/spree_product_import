Spree::Core::Engine.add_routes do
  namespace :admin do
    get '/products/import', to: "products#import", as: :import_products
    post '/products/import', to: "products#create_import", as: :create_imported_product
  end
end
