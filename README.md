= SpreeProductImport

```shell
bundle exec rails g spree_product_import:install
bundle exec rake db:migrate
```

### Local development

To build and install the gem, add the following to your project's `Gemfile`

```rb
gem 'spree_product_import', :path => '../spree_product_import'
```

Then run

```shell
bundle install
```
