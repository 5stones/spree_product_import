Spree::Admin::ProductsController.class_eval do

  # GET /admin/products/import
  def import
    @csv_format = Spree::ProductImportConfiguration[:csv_format]
  end

  # POST /admin/products/import
  def create_import
    @csv_format = Spree::ProductImportConfiguration[:csv_format]
    import_result = Spree::Product.import(params[:csv])
    flash[:success] = import_result['success'].to_s + ' Products successfully imported'
    @errors = import_result['failure']
    render action: 'import'
  end
end
