require 'csv'

class ProductImportError < StandardError
end

Spree::Product.class_eval do
  # Import a collection of products into Spree using a csv
  def self.import(file)
    errors = []
    success_count = 0
    rows = Spree::ProductImportConfiguration[:csv_format]

    CSV.foreach(file.path) do |row|

      data = { taxonomies: [], properties: [] }

      # build the data object from the csv
      rows.each_with_index do |key, index|
        # check for taxonomies
        if(key.key?(:type) && key[:type] == Spree::Taxonomy)
          data[:taxonomies].push({ taxonomy: key[:name], taxons: row[index] })
        elsif(key.key?(:type) && key[:type] == Spree::Property)
          data[:properties].push({ name: key[:name], value: row[index] })
        else
          data[key[:name]] = row[index]
        end
      end

      # pull out the sku
      sku = data[:sku]
      data.delete(:sku)

      sku_errors = []
      sku_errors[0] = sku
      sku_errors[1] = []

      begin
        import_one(sku, data)
        success_count += 1
      rescue ProductImportError, ActiveRecord::RecordInvalid => e
        sku_errors[1] << e.message.to_s
      end

      errors << sku_errors if sku_errors[1].length > 0
    end

    result = Hash.new
    result['success'] = success_count

    if errors.length > 0
      result['failure'] = errors
    end

    return result
  end

  # Import a single product into Spree by SKU
  def self.import_one(sku, data)

    variant = Spree::Variant.find_by sku: sku
    if variant
      product = variant.product
    end
    # this can't just be an else because sometimes there is a variant but not a product for it to be attached to
    # (likely due to an earlier error with creating the same product)
    if !product
      product = new()
    end

    product.sku = sku
    product.shipping_category_id = 1
    if !product.id
      product.available_on = Date.today
    end

    product.process_data(data)

    product.import_taxonomies(data)
    result = product.save!
    product.import_properties(data)
    Rails.logger.info "************* Finished Import on product: #{product.sku} *************"
    return result
  end

  # import all provided fields
  def process_data(data)
    # data = data.stringify_keys!

    # process all non taxonomy or property cells
    data.each do |key, value|
      next if key === :taxonomies || key === :properties
      import_method = ('import_' + key.to_s).to_sym

      if(self.respond_to? import_method)
        self.send(import_method, key, value)
      else
        # Assume a straight assignment operation
        self.send(key.to_s + "=", value)
      end

    end
  end

  # Add product to taxons as a part of the import
  def import_into_taxons(taxonomy_key, taxon_codes)

    taxonomy_name = taxonomy_key.to_s.humanize

    if taxon_codes && taxon_codes.length > 0
      root_taxonomy = Spree::Taxonomy.find_or_create_by(name: taxonomy_name)
      root_taxon = root_taxonomy.root
      taxon_code_list = taxon_codes.split ','

      taxon_code_list.each do |taxon_code|
        taxon_code = taxon_code.strip

        taxon = Spree::Taxon.find_by(meta_keywords: taxon_code, taxonomy: root_taxonomy)
        taxon ||= Spree::Taxon.find_by(name: taxon_code.to_s.humanize, taxonomy: root_taxonomy)
        taxon ||= Spree::Taxon.find_or_create_by(name: taxon_code.to_s.humanize, meta_keywords: taxon_code, taxonomy: root_taxonomy, parent: root_taxon)

        if taxon && taxon.taxonomy.name == root_taxon.name
          while taxon && (taxon.id != root_taxon.id)
            taxons << taxon unless taxons.include? taxon
            taxon = taxon.parent
          end
        end
      end

    end
  end

  # process taxonomies
  def import_taxonomies(data)
    if data[:taxonomies].length > 0
      data[:taxonomies].each do |value|
        import_into_taxons(value[:taxonomy], value[:taxons])
      end

      # remove key
      data.delete(:taxonomies)
    end
  end

  # process properties
  def import_properties(data)
    if data[:properties].length > 0
      data[:properties].each do |value|

        if !(value[:value].nil? || value[:value].empty?)
          set_property(value[:name].to_s.humanize, value[:value])
        end
      end

      # remove key
      data.delete(:properties)
    end
  end

  # import image by url
  def import_image_url(key, value)
    image_uri = value
    if self.images.length == 0
      begin
        self.images << Spree::Image.create!({
              :attachment => open(URI.parse(image_uri).to_s),
              :viewable => self
            })
      rescue OpenURI::HTTPError => e
        Rails.logger.info "Image not found for product " + sku
      end
    end
  end
end
