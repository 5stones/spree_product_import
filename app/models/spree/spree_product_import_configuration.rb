module Spree
  class SpreeProductImportConfiguration < Preferences::Configuration
    preference :csv_format, :array, default: [
      { name: :sku, type: String },
      { name: :image_name, type: String },
      { name: :meta_title, type: String },
      { name: :meta_keywords, type: String },
      { name: :meta_description, type: String }
    ]
  end
end
