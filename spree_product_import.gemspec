$:.push File.expand_path("../lib", __FILE__)

Gem::Specification.new do |s|
  s.name        = "spree_product_import"
  s.version     = '0.0.1'
  s.authors     = ["Chas Mrozek", "Jacob Spizziri"]
  s.email       = ["chas.mrozek@lighthousecatholicmedia.org", "jacob.spizziri@lighthousecatholicmedia.org"]
  s.summary     = "Product import tool for Spree Commerce"
  s.description = "Used for syncing product information from CSV's into the Spree Commerce Admin"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", ">= 4.2"

  s.add_dependency "spree_core", ">= 3.0", "< 4.0"

end
